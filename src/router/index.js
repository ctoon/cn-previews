import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'

import Latest from '@/components/Latest'
import Show from '@/components/Show'
import Details from '@/components/Details'
import ShowList from '@/components/ShowList'
import About from '@/components/About'

Vue.use(Router)
Vue.use(Meta)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Latest',
      component: Latest
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/shows',
      name: 'ShowList',
      component: ShowList
    },
    {
      path: '/shows/:id',
      name: 'Show',
      component: Show
    },
    {
      path: '/details/:id',
      name: 'Details',
      component: Details
    }
  ]
})
