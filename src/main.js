/* globals VERSION, COMMITHASH, BRANCH, BACKENDURL */
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
// Polyfills for IE10+ and Safari 6.1+, now stop crying
import Promise from 'promise-polyfill'
import 'whatwg-fetch'
// Raven to use with Sentry.io
import Raven from 'raven-js'
import RavenVue from 'raven-js/plugins/vue'
// Some cool plugins and styles
import VueAnalytics from 'vue-analytics'
import VueHighlightJS from 'vue-highlightjs'
import {
  Vuetify,
  VApp,
  VGrid,
  VIcon,
  VToolbar,
  VCard,
  VAlert,
  VFooter,
  VBtn,
  VSpeedDial,
  VTooltip,
  VExpansionPanel,
  VSnackbar,
  transitions
} from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'mdi/css/materialdesignicons.min.css'
// Our App, router and compnents
import App from './App'
import router from './router'
import PreviewDetails from '@/components/PreviewDetails'

// Apply polyfill
if (!window.Promise) {
  window.Promise = Promise
}

Vue.use(Vuetify, {
  components: {
    VApp,
    VGrid,
    VIcon,
    VToolbar,
    VCard,
    VAlert,
    VFooter,
    VBtn,
    VSpeedDial,
    VTooltip,
    VExpansionPanel,
    VSnackbar,
    transitions
  },
  theme: {
    primary: '#7fdbff', // This is used for the link colors
    secondary: '#ffeb00'
  }
})
Vue.config.productionTip = false

Vue.component('preview-details', PreviewDetails)

// Enable plugins
Raven
  .config('https://9ea34fa4b8a446cdb6c0820e434cd60b@sentry.io/243750', {
    release: VERSION,
    tags: {
      git_commit: COMMITHASH,
      git_branch: BRANCH
    },
    environment: process.env
  })
  .addPlugin(RavenVue, Vue)
  .install()
Vue.use(VueAnalytics, {
  id: 'UA-103935709-4',
  router
})
Vue.use(VueHighlightJS)

// Say hi to whoever opens the console
console.log(
  '%cCN Previews',
  'font-size:70px;color:#fff;text-shadow:0 1px 0 #ccc,0 2px 0 #c9c9c9,0 3px 0 #bbb,0 4px 0 #b9b9b9,0 5px 0 #aaa,0 6px 1px rgba(0,0,0,.1),0 0 5px rgba(0,0,0,.1),0 1px 3px rgba(0,0,0,.3),0 3px 5px rgba(0,0,0,.2),0 5px 10px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.2),0 20px 20px rgba(0,0,0,.15);'
)
console.log(
  '%cby CTOON Webmedia Group' +
  '\nGit: https://gitlab.com/ctoon/cn-previews',
  'font-style:italic;'
)
console.log(
  '%cCommit: ' + COMMITHASH +
  '\nBranch: ' + BRANCH,
  'color:green;'
)

// Create vue app
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  data: {
    backendurl: BACKENDURL,
    errorAlert: {
      show: false,
      message: 'Something, somewhere, went wrong... Blame the developpers.'
    }
  }
})
