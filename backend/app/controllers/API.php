<?php
class API {
  public function beforeRoute($f3) {
    $db = $f3->get('db');
    $f3->set('shows', new DB\SQL\Mapper($db, 'shows'));
    $f3->set('previews', new DB\SQL\Mapper($db, 'previews'));
  }

  public function error($f3) {
    $output = array('error' => array(
      'code' => $f3->get('ERROR.code'),
      'message' => $f3->get('ERROR.text')
    ));

    echo json_encode($output, JSON_PRETTY_PRINT);
  }

  public function latest($f3) {
    $perpage = 15;
    $offset = 0;
    $output = [];
    $res = $f3->get('previews');

    if ($f3->exists('PARAMS.page')) {
      $offset = ($f3->get('PARAMS.page') - 1) * $perpage;
    }

    $filter = array();

    if ($f3->get('QUERY') === 'future') {
      $filter = array('airdate > ?', time());
    } elseif ($f3->get('QUERY') === 'past') {
      $filter = array('airdate < ? OR airdate IS NULL', time());
    }

    $res->load(
      $filter,
      array(
        'order' => 'id DESC',
        'limit' => $perpage,
        'offset' => $offset
      )
    );

    while(!$res->dry()) {
      $entry = $res->cast();
      $f3->get('shows')->load(
        array('id=?', $entry['showid'])
      );
      $entry['show'] = $f3->get('shows')->name;
      $entry['videos'] = explode(',', $entry['videos']);
      $entry['images'] = explode(',', $entry['images']);
      unset($entry['xml']);

      // Order by key
      ksort($entry);

      $output[] = $entry;
      $res->next();
    }

    echo json_encode($output, JSON_PRETTY_PRINT);
  }

  public function show($f3) {
    $perpage = 15;
    $offset = 0;
    $output = [];
    $res = $f3->get('previews');

    if ($f3->exists('PARAMS.page')) {
      $offset = ($f3->get('PARAMS.page') - 1) * $perpage;
    }

    $f3->get('shows')->load(
      array('id=?', $f3->get('PARAMS.id'))
    );

    if ($f3->get('shows')->dry()) {
      $f3->error(404, 'Nothing available yet at this showid.');
      return;
    }

    $res->load(
      array('showid=?', $f3->get('shows')->id),
      array(
          'order' => 'id DESC',
          'limit' => 15,
          'offset' => $offset
      )
    );

    while(!$res->dry()) {
      $entry = $res->cast();
      $f3->get('shows')->load(
        array('id=?', $entry['showid'])
      );
      $entry['show'] = $f3->get('shows')->name;
      $entry['videos'] = explode(',', $entry['videos']);
      $entry['images'] = explode(',', $entry['images']);
      unset($entry['xml']);

      // Order by key
      ksort($entry);

      $output[] = $entry;
      $res->next();
    }

    echo json_encode($output, JSON_PRETTY_PRINT);
  }

  public function shows($f3) {
    $output = [];
    $res = $f3->get('shows');

    $res->load(
      array(),
      array(
        'order' => 'lastupdate DESC',
        'limit' => 99
      )
    );

    while(!$res->dry()) {
      $output[] = $res->cast();
      $res->next();
    }

    echo json_encode($output, JSON_PRETTY_PRINT);
  }

  public function details($f3) {
    $f3->get('previews')->load(
      array('id=?', $f3->get('PARAMS.id'))
    );

    if ($f3->get('previews')->dry()) {
      $f3->error(404, 'There isn\'t any preview available at this ID.');
      return;
    }

    $entry = $f3->get('previews')->cast();
    $f3->get('shows')->load(
      array('id=?', $entry['showid'])
    );
    $entry['show'] = $f3->get('shows')->name;
    $entry['videos'] = explode(',', $entry['videos']);
    $entry['images'] = explode(',', $entry['images']);

    // Order by key
    ksort($entry);

    echo json_encode($entry, JSON_PRETTY_PRINT);
  }
}
