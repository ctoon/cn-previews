<?php
class Admin {
  public function beforeRoute($f3) {
    $db = $f3->get('db');
    $f3->set('keys', new DB\SQL\Mapper($db, 'keys'));
    $f3->set('shows', new DB\SQL\Mapper($db, 'shows'));
    $f3->set('previews', new DB\SQL\Mapper($db, 'previews'));
  }

  public function home($f3) {
    // Redirect to API homepage
    $f3->reroute('https://api.ctoon.network/');
  }

  public function submit($f3) {
    // Check if key is valid
    $keys = $f3->get('keys');
    $keys->load(
      array('value=?', $f3->get('HEADERS.X-Api-Key'))
    );
    if ($keys->dry()) {
      $f3->error(403);
      return;
    }

    // Read body (json) input
    $json = json_decode($f3->get('BODY'));

    // Check for show subs
    foreach(explode(',', $_ENV['SUBS']) as $s) {
      if (preg_match('/' . $s . '/', $json->show)) {
        $json->show = $s;
      }
    }

    // Get show id
    $shows = $f3->get('shows');
    $shows->load(
      array('name=?', $json->show)
    );
    // If it doens't exist, create it
    if ($shows->dry()) {
      $shows->reset();
      $shows->name = $json->show;
    }
    // Update timestamp
    $shows->lastupdate = time();
    $shows->save();

    // Add episode data
    $previews = $f3->get('previews');
    $previews->load(
      array('id=?', $json->id)
    );
    // ... if it doesn't exists yet
    if (!$previews->dry()) {
      echo json_encode(array(
        'result' => 'Already exists.'
      ));
    } else {
      $previews->reset();
      $previews->id = $json->id;
      $previews->showid = $shows->id;
      $previews->title = $json->title;
      $previews->description = $json->description;
      $previews->airdate = $json->airdate;
      $previews->finddate = $json->finddate;
      $previews->images = $json->images;
      $previews->videos = $json->videos;
      $previews->xml = $json->xml;
      $previews->save();

      echo json_encode(array(
        'result' => 'Saved!'
      ));

      // Discord webhook
      $webhook = array(
        'embeds' => array(
          array(
            'title' => $json->title,
            'description' => $json->description,
            'color' => 16771840, // #FFEB00
            'url' => 'https://previews.ctoon.network/details/' . $json->id,
            'footer' => array(
              'text' => $json->show
            ),
            'thumbnail' => array(
              'url' => explode(',', $json->images)[0]
            )
          )
        )
      );
      if ($json->airdate) $webhook['embeds']['timestamp'] = date('c', $json->airdate);
      $webhook_string = json_encode($webhook);

      foreach (explode(',', $_ENV['DISCORD']) as $url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $webhook_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($webhook_string))
        );
        curl_exec($ch);
      }
    }
  }
}
