/*
 Date: 11/11/2017 23:44:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for keys
-- ----------------------------
DROP TABLE IF EXISTS `keys`;
CREATE TABLE `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `indexKeys` (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for previews
-- ----------------------------
DROP TABLE IF EXISTS `previews`;
CREATE TABLE `previews` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'same as the XML id',
  `showid` int(11) NOT NULL COMMENT 'let''s just throw here the name',
  `title` varchar(255) NOT NULL COMMENT 'episode title',
  `description` text COMMENT 'synopsis',
  `airdate` bigint(20) DEFAULT NULL COMMENT 'when it airs on tv',
  `finddate` bigint(20) NOT NULL COMMENT 'auto updated with new data',
  `images` longtext COMMENT 'comma delemited',
  `videos` longtext COMMENT 'comma delemited',
  `xml` longtext NOT NULL COMMENT 'original file',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fkShowsId` (`showid`),
  CONSTRAINT `fkShowsId` FOREIGN KEY (`showid`) REFERENCES `shows` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1082556 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for shows
-- ----------------------------
DROP TABLE IF EXISTS `shows`;
CREATE TABLE `shows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `lastupdate` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `indexShow` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

SET FOREIGN_KEY_CHECKS = 1;
