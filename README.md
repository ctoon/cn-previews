# CN Previews

[![Standard - JavaScript Style Guide](https://img.shields.io/badge/code%20style-standard-green.svg)](http://standardjs.com/)
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgitlab.com%2Fctoon%2Fcn-previews.svg?type=shield)](https://app.fossa.io/projects/git%2Bgitlab.com%2Fctoon%2Fcn-previews?ref=badge_shield)
[![Gitlab Build Status](https://gitlab.com/ctoon/cn-schedule/badges/master/build.svg)](https://gitlab.com/ctoon/cn-schedule/commits/master)

> [Previews from CN shows](https://previews.ctoon.network/)


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


## Backend

```bash
# go to the backend folder
cd backend

# install dependencies
composer install

# copy and edit environement configuration
cp .env.example .env
$EDITOR .env

# install schema
mysql -u username -p < schema.sql
```
